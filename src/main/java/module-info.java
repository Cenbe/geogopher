module org.lyonlabs.geogopher {
  requires org.apache.logging.log4j.core;
  requires org.apache.logging.log4j;
  requires org.lyonlabs.d64;
  requires org.lyonlabs;
  exports org.lyonlabs.geogopher.gophermap;
}
