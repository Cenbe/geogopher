/**************************************************************************
  ConnectionThread.java
 **************************************************************************/

package org.lyonlabs.geogopher;

import org.lyonlabs.geogopher.gophermap.GopherItem;
import org.lyonlabs.geogopher.gophermap.GopherInfoItem;
import org.lyonlabs.geogopher.gophermap.GopherErrorItem;
import org.lyonlabs.geogopher.gophermap.SearchItem;
import java.io.*;
import java.net.*;
import java.nio.charset.*;
import java.nio.file.*;
import java.util.*;
import java.util.stream.*;
import org.apache.logging.log4j.*;
import org.lyonlabs.Lyonlabs;
import org.lyonlabs.d64.*;
import org.lyonlabs.d64.image.*;
import org.lyonlabs.d64.utility.*;
import org.lyonlabs.geogopher.gophermap.GopherItem.GopherType;

/**
 * Listener thread for a single gopher request.
 */
public class ConnectionThread implements Runnable {
  private final Logger logger = LogManager.getLogger(getClass().getName());
  private final ConnectionListener listener;
  private final Socket socket;
  private final String doubleSeparator = File.separator + File.separator;
  private final byte[] dollar = new byte[] { '\t', '$', '\r', '\n' };
  
  /**
   * Constructor.
   * @param listener The connection listener creating this thread.
   * @param socket The client socket.
   */
  public ConnectionThread(ConnectionListener listener, Socket socket) {
    this.listener = listener;
    this.socket = socket;
  }

  @Override
  public void run() {
    String selector;
    List<String> dump;
    byte[] inBytes = new byte[256];
    int msgLen;
    boolean reading = true, sendDefault = false;

    try (InputStream is = socket.getInputStream();
         OutputStream os = socket.getOutputStream();
         PrintWriter pw = new PrintWriter(os, true)) {
      while (reading) {
        msgLen = is.read(inBytes);
        dump = Lyonlabs.hexDump(inBytes, 0, msgLen);
        for (String line : dump) {
          logger.debug(line);
        }

        try {
          if (msgLen == 2 
          &&  (inBytes[msgLen - 2] == '\r' && inBytes[msgLen - 1] == '\n')) {
            logger.debug("received \\r\\n");
            sendDefault = true;
          } else if (msgLen == 1 &&  inBytes[0] == '\r') {
            logger.debug("received \\r");
            sendDefault = true;
          } else if (msgLen == 1 && inBytes[0] == '\t') {
            // first char of dollar string sent on different request
            logger.info("single tab character received, continuing");
            continue;  // NOTE: reading not set to false            
          } else if (msgLen == dollar.length 
          &&  Arrays.equals(Arrays.copyOfRange(inBytes, 0, 4), dollar)) {
            // some gopher clients will send the "dollar string" (gopher+ ?)
            logger.debug("full dollar string received");
            sendDefault = true;
          } else if (msgLen == dollar.length - 1
          && Arrays.equals(Arrays.copyOfRange(inBytes, 0, 3), 
                           Arrays.copyOfRange(dollar, 1, 4))) {
            // first char of dollar string sent previously (see above)
            logger.debug("last three chars of dollar string received");
            sendDefault = true;
          }
          if (sendDefault) {
            sendGopherMap(listener.getRootGopherMap(), pw);
            reading = false;
          }

          if (reading) {
            // strip cr/lf
            if (msgLen >= 2 
            &&  (inBytes[msgLen - 2] == '\r' && inBytes[msgLen - 1] == '\n')) {
              selector = new String(
                         Arrays.copyOfRange(inBytes, 0, msgLen - 2), 
                         StandardCharsets.US_ASCII);
            } else if (msgLen >= 1 &&  inBytes[msgLen - 1] == '\r') {
              selector = new String(
                         Arrays.copyOfRange(inBytes, 0, msgLen - 1),
                         StandardCharsets.US_ASCII);
            } else {
              selector = new String(inBytes, 0, msgLen, StandardCharsets.US_ASCII);
            }
            logger.info("selector evaluates to:");
            dump = Lyonlabs.hexDump(selector.getBytes(), 0, selector.length());
            for (String line : dump) {
              logger.debug(line);
            }
            logger.info(socket.getInetAddress() + " sends selector \""
                      + selector + "\"");

            // Note the assumption here: the gophermaps for this server
            // must be in RFC 1436 format.
            if (selector.contains("\t")) {
              doSearch(selector, pw);
              reading = false;
            } else {
              sendResource(selector, os, pw);
              reading = false;
            }
          }
        } catch (Exception exc) {
          new GopherErrorItem(exc.getMessage()).write(pw);
          throw exc;
        }
      }  // while (reading)
    } catch (Exception exc) {
      logger.error(exc.getMessage(), exc);
    } finally {  // auto-close
      logger.info("client thread " + Thread.currentThread().getName() 
                + " ending, closing socket.");
    }
  }
  
  private void sendResource(String selector, OutputStream os, PrintWriter pw)
               throws Exception {
    File selected, gopherMap;
    String fileName = "";
    
    // make relative path absolute
    if (!selector.startsWith("/") 
    &&  !selector.startsWith(listener.getRootGopherDir().getPath())) {
      selector = listener.getRootGopherDir().getPath() + "/" + selector;
    }

    // Selectors representing files within disk images are formatted as 
    // ...imageName//fileName.
    if (selector.contains(doubleSeparator)) {
      if (selector.startsWith(doubleSeparator)
      ||  selector.endsWith(doubleSeparator)) {
        new GopherErrorItem("Invalid selector!").write(pw);
        throw new Exception("Invalid selector: " + selector);
      } else {
        selected = new File(selector.substring(0, 
                            selector.indexOf(doubleSeparator)));
        fileName = selector.substring(selector.indexOf(doubleSeparator) + 2);
      }
    } else {
      if (selector.equals("/")) {
        selected = listener.getRootGopherDir();
      } else {
        // path (relative or absolute) or file in current directory
        selected = new File(selector);
      }
    }
    if (!selected.exists() || !selected.canRead()) {
      new GopherErrorItem("Can't locate resource!").write(pw); 
      throw new Exception("Can't locate " + selector + "!");
    }

    if (selector.contains(doubleSeparator)) {
      if (isDiskImage(selected)) {  // FIXME assume GEOS? rut roh
        sendGeosFile(selected, fileName, os);
      }
    } else {
      if (isDiskImage(selected)) {
        gopherMap = new File(selected.getPath() + ".gophermap");
        if (gopherMap.exists() && gopherMap.isFile() && gopherMap.canRead()) {
          sendGopherMap(gopherMap, pw);
        } else {
          logger.warn("no gophermap found, synthesizing from files in image");        
          sendImageDirectory(selected, pw);
        }
      } else {
        if (selected.isDirectory()) {
          gopherMap = new File(selected, listener.getGopherMapName());
          if (gopherMap.exists() && gopherMap.isFile() && gopherMap.canRead()) {
            sendGopherMap(gopherMap, pw);
          } else {
            logger.warn("no gophermap found, synthesizing from images in dir");
            sendDirectory(selected, pw);
          }
        } else {          
          sendFile(selected, os);  // client should know what to do
        }
      }
    }
  }
  
  /**
   * Send a gophermap to the client.
   * @param  gopherMap The gophermap to send.
   * @param  pw A PrintWriter on the client socket.
   * @throws Exception 
   */
  private void sendGopherMap(File gopherMap, PrintWriter pw) 
               throws Exception {
    String item;
    
    logger.info("sending gophermap: " + gopherMap);
    try (BufferedReader br = new BufferedReader(
         new FileReader(gopherMap))) {
      while ((item = br.readLine()) != null) {
        pw.print(item + "\r\n");
      }
    }
  }
  
  /**
   * Synthesize directory of disk images when no gophermap is present.
   * @param  selected The directory to create a listing for.
   * @param  pw The socket writer.
   * @throws Exception 
   */
  private void sendDirectory(File selected, PrintWriter pw) throws Exception {
    Set<String> allSuffixes;
    Stream<Path> files;
    List<GopherItem> items;
    
    allSuffixes = D64Utility.getImageSuffixes();
    files = Files.list(selected.toPath())
                 .filter(p -> allSuffixes.contains(
                         p.toString().substring(p.toString().length() - 3)
                          .toUpperCase()));
    items = new ArrayList<>();
    files.forEach(p -> addImageItem(p, items));
    if (items.isEmpty()) {
      new GopherErrorItem("Empty directory!").write(pw);
    } else {
      new GopherItem(GopherType.DIR, "parent directory",
                     selected.getParentFile().toString(),
                     listener.getHostName(), listener.getPort())
          .write(pw);
      for (GopherItem item : items) {
        item.write(pw);
      }
    }
  }
  
  private void addImageItem(Path path, List<GopherItem> items) {
    String selector, rootString;
    
    selector = path.toString();
    rootString = listener.getRootGopherDir().toString();
    if (selector.startsWith(rootString)) {
      selector = selector.substring(rootString.length());
    }
    items.add(new GopherItem(GopherType.DIR, 
                             path.getFileName().toString(), selector,
                             listener.getHostName(), listener.getPort()));
  }
  
  private boolean isDiskImage(File selected) {
    try {
      D64Utility.findImageType(selected);
    } catch (Exception exc) {
      return false;
    }
    return true;
  }
  
  private void sendFile(File selected, OutputStream os) throws Exception {
    logger.debug("sendFile: " + selected);
    os.write(Files.readAllBytes(selected.toPath()));
  }
  
  /**
   * Send a list of gopher items built from the directory entries of a disk 
   * image.
   * @param  selected The selected disk image.
   * @param  pw A print writer on the client socket's output stream.
   * @throws Exception 
   */
  private void sendImageDirectory(File selected, PrintWriter pw) 
               throws Exception {
    DiskImage diskImage;
    List<DirEntry> dirEntries;
    GeosFileHeader header;
    String parentDir, rootDir, fileName, fileInfo, displayString;
    
    diskImage = DiskImageFactory.diskImageFromFile(selected);
    logger.info("sending image directory for " + selected);
    parentDir = selected.getParent();
    rootDir = listener.getRootGopherDir().getPath();
    if (parentDir == null) {
      parentDir = "/";
    }
    new GopherItem(GopherType.DIR, "parent directory",
        parentDir, listener.getHostName(), listener.getPort()).write(pw);
    dirEntries = diskImage.getDirectory();
    for (DirEntry dirEntry : dirEntries) {
      fileName = dirEntry.getFileName();
      displayString = fileName;
      header = dirEntry.getGeosFileHeader();
      if (header != null) {
        fileInfo = header.getPlainInfo();
        if (!fileInfo.trim().isEmpty()) {
          displayString += ": " + fileInfo.replace('\r', ' ');
        }
      }
      logger.debug("sending selector for " + fileName);
      new GopherItem(GopherType.BINARY, displayString,
                     selected.getPath() + doubleSeparator + fileName,
                     listener.getHostName(), listener.getPort()).write(pw);
    }
  }
  
  private void sendGeosFile(File imageFile, String geosFileName, 
                            OutputStream os) throws  Exception {
    DiskImage diskImage;
    DirEntry dirEntry;
    
    if (!imageFile.exists()) {
      throw new Exception(imageFile.getPath() + " not found!");
    }
    diskImage = DiskImageFactory.diskImageFromFile(imageFile);
    logger.info("sending " + geosFileName + " from " + imageFile.getName());
    dirEntry = diskImage.findDirEntry(geosFileName, D64Utility.ASCII);
    diskImage.streamGeosFileAsCvt(dirEntry, os);    
  }
  
  private void doSearch(String selector, PrintWriter pw) throws Exception {
    String startDir, searchText;
    String[] searchTexts;
    List<String> searchTerms;
    StringBuilder sb;
    Stream<Path> searchDirs;
    SortedSet<GopherItem> items = new TreeSet<>();
    
    String[] s = selector.split("\t");
    if (s.length == 0 || s.length > 2) {
      throw new Exception("Invalid search selector!");
    }
    startDir = s[0];
    searchText = s[1];
    searchTexts = searchText.trim().split(" ");
    searchTerms = new ArrayList<>();
    sb = new StringBuilder();
    for (int i = 0; i < searchTexts.length; i++) {
      if (searchTexts[i].equals("")) {
        continue;
      }
      searchTerms.add(searchTexts[i]);
      sb.append("'").append(searchTexts[i]).append("' ");
    }
    logger.debug("doSearch, dir: " + startDir 
               + ", search terms: " + sb.toString());
    
    searchDirs = Files.walk(Paths.get(startDir), FileVisitOption.FOLLOW_LINKS)
                .filter(p -> Files.isDirectory(p));
    try {
      for (Path searchDir : (Iterable<Path>)searchDirs::iterator) {
        searchGophermaps(searchDir, searchTerms, items, pw);
      }
      searchImages(startDir, searchTerms, items, pw);
    } catch (Exception exc) {
      new GopherErrorItem(exc.getMessage()).write(pw);
    }
  }
  
  private void searchGophermaps(Path searchDir, List<String> searchTerms, 
               SortedSet<GopherItem> items, PrintWriter pw) throws Exception {
    
    logger.debug("searching gophermap in " + searchDir);
    
    // check the gophermap; if it's in there, add a directory item
    Path gophermap = Paths.get(searchDir.toString(), 
                               listener.getGopherMapName());
    if (Files.exists(gophermap)) {
      for (String line : Files.readAllLines(gophermap)) {
        GopherItem item = new GopherItem(line);
        if (item.getItemType() == GopherType.DIR) {
          String displayString = item.getDisplayString().toUpperCase();
          if (displayString.toUpperCase().equals("PARENT DIRECTORY")) {
            continue;  // hope there are no real dirs with this name :)
          }
          for (String searchTerm : searchTerms) {
            if (searchTerm.equals("")) {
              continue;
            }
            if (displayString.contains(searchTerm.toUpperCase())) {
              items.add(item);
              break;
            }
          }
        }
      }
    }
  }
  
  private void searchImages(String startDir, List<String> searchTerms, 
               SortedSet<GopherItem> items, PrintWriter pw) throws Exception {
    Stream<Path> files;
    Set<String> allSuffixes = D64Utility.getImageSuffixes();
    SortedSet<SearchItem> searchItems = new TreeSet<>();

    files = Files.walk(Paths.get(startDir), FileVisitOption.FOLLOW_LINKS)
                 .filter(p -> allSuffixes.contains(
                         p.toString().substring(p.toString().length() - 3)
                          .toUpperCase()));
    files.forEach(f -> searchImage(f, searchTerms, searchItems));
    try {
      items.addAll(searchItems);
    } catch (Exception exc) {
      logger.error(exc.getMessage(), exc);
    }
    new GopherItem(GopherType.DIR, "return to search directory",
                   startDir, listener.getHostName(), 
                   listener.getPort()).write(pw);
    if (items.isEmpty()) {
      new GopherInfoItem("No results found.").write(pw);
    } else {
      logger.debug(items.size() + " items found.");
      if (items.size() > 250) {
        throw new Exception("Too many results, refine your search.");
      }
      for (GopherItem searchItem : items) {
        searchItem.write(pw);
      }
    }
  }

  /**
   * Search within a disk image.
   * @param path The path of the disk image.
   * @param searchTerms The string(s) to search for.
   * @param searchItems An set of search items to add to.
   */
  private void searchImage(Path path, List<String> searchTerms,
                           SortedSet<SearchItem> searchItems) {                          
    DiskImage diskImage;
    DirHeader dirHeader;
    List<DirEntry> dirEntries;
    String fileName, fileInfo, permanentNameString, displayString;
    GeosFileHeader fileHeader;
    boolean found = false;
    
    try {
      diskImage = DiskImageFactory.diskImageFromFile(path.toFile());
      dirHeader = diskImage.getDirHeader();
      dirEntries = diskImage.getDirectory();
      for (DirEntry dirEntry : dirEntries) {
        if (dirEntry.isGeosFile()) {
          fileHeader = dirEntry.getGeosFileHeader();
          fileInfo = fileHeader.getInfo();
          permanentNameString = fileHeader.getPermanentNameString();
        } else {
          fileInfo = "";
          permanentNameString = "";
        }
        fileName = dirEntry.getFileName();
        found = false;
        for (String searchTerm : searchTerms) {
          if (fileName.toUpperCase().contains(searchTerm.toUpperCase())) {
            found = true;
            break;
          }
        }
        if (!found && dirEntry.isGeosFile()) {
          for (String searchTerm : searchTerms) {
            if (fileInfo.toUpperCase().contains(searchTerm.toUpperCase())) {
              found = true;
              break;
            }
          }
        }
        if (found) {
          logger.info("found file " + dirEntry.getFileName() 
                    + " on disk \"" + dirHeader.getDiskName().trim() 
                    + "\" in image " + path.getFileName());
          if (fileInfo.trim().isEmpty()) {
            displayString = fileName;
          } else {
            displayString = fileName + ": " + fileInfo.replace('\r', ' ');
          }
          searchItems.add(new SearchItem(GopherType.BINARY, displayString,
                     permanentNameString, path + doubleSeparator + fileName,
                     listener.getHostName(), listener.getPort()));
        }
      }
    } catch (Exception exc) {
      logger.error(exc.getMessage(), exc);
    }
  }  
}
