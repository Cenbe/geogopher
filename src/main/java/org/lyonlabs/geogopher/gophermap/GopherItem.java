/**************************************************************************
  GopherItem.java
 **************************************************************************/

package org.lyonlabs.geogopher.gophermap;

import java.io.*;

/**
 * Class representing an item in a gophermap.
 */
public class GopherItem implements Comparable {

  public enum GopherType {
    TEXT('0'),
    DIR('1'),
    ERROR('3'),
    SEARCH('7'),
    BINARY('9'),
    INFO('i'),
    BOGUS('~');  // for testing
    
    private char type;
    
    GopherType(char type) {
      this.type = type;
    }
    
    public char getType() {
      return type;
    }
  }
  private final GopherType itemType;
  private final String displayString, selector, hostname;
  private final int port;  

  public GopherItem(GopherType itemType, String displayString, 
                    String selector, String hostname, int port) {
    this.itemType = itemType;
    this.displayString = displayString;
    this.selector = selector;
    this.hostname = hostname;
    this.port = port;
  }

  public GopherItem(String line) throws Exception {
    GopherType itemType = null;
    String[] parts = line.split("\t");
  
    if (parts.length != 4) {
      throw new Exception("Invalid gophermap line!");
    }
    for (GopherType type : GopherType.values()) {
      if (parts[0].charAt(0) == (type.getType())) {
        itemType = type;
        break;
      }
    }
    if (itemType == null) {
      throw new Exception("Invalid gophermap line!");
    } else {
      this.itemType = itemType;
    }
    this.displayString = parts[0].substring(1);
    this.selector = parts[1];
    this.hostname = parts[2];
    this.port = Integer.valueOf(parts[3]);
  }
  
  public void write(Writer writer) throws IOException {
    writer.append(itemType.getType()).append(displayString).append("\t")
          .append(selector).append("\t")
          .append(hostname).append("\t")
          .append(String.valueOf(port))
          .append((char)(0x0d)).append((char)(0x0a));
  }
  
  @Override
  public String toString() {
    return this.getDisplayString();
  }
  
  @Override
  public boolean equals(Object obj) {
    if (!(obj instanceof GopherItem)) {
      return false;
    }
    GopherItem that = (GopherItem)obj;
    return getCompareString().equals(that.getCompareString());
  }
  
  @Override
  public int hashCode() {
    int hash = 3;
    hash = 17 * hash + this.getDisplayString().hashCode();
    hash = 17 * hash + this.getSelector().hashCode();
    return hash;
  }

  @Override
  public int compareTo(Object obj) {
    GopherItem that;
    int result;
    
    if (obj instanceof GopherItem) {
      that = (GopherItem)obj;
    } else {
      throw new ClassCastException();
    }
    
    return this.getCompareString().compareTo(that.getCompareString());
  }
  
  public String getCompareString() {
    return String.valueOf(this.getItemType().getType()) 
         + this.getDisplayString();
  }
  
  public GopherType getItemType() {
    return itemType;
  }

  public String getDisplayString() {
    return displayString;
  }

  public String getSelector() {
    return selector;
  }

  public String getHostname() {
    return hostname;
  }

  public int getPort() {
    return port;
  }
}
