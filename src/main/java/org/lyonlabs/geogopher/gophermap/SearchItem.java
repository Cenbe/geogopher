/**************************************************************************
  SearchItem.java
 **************************************************************************/

package org.lyonlabs.geogopher.gophermap;

/**
 * Subclass of GopherItem which includes permanent name string for
 * detecting duplicate search results.
 */
public class SearchItem extends GopherItem {
  String permanentNameString;
  
  public SearchItem(GopherType itemType, String displayString, 
                    String permanentNameString, String selector, 
                    String hostname, int port) {
    super(itemType, displayString, selector, hostname, port);
    this.permanentNameString = permanentNameString;
  }
  
  @Override
  public boolean equals(Object obj) {
    if (!(obj instanceof SearchItem)) {
      return false;
    }
    
    SearchItem that = (SearchItem)obj;
    return this.getDisplayString().equals(that.getDisplayString())
        && this.getPermanentNameString().equals(that.getPermanentNameString());
  }
  
  @Override
  public int hashCode() {
    int hash = 3;
    hash = 17 * hash + this.getDisplayString().hashCode();
    hash = 17 * hash + this.getPermanentNameString().hashCode();
    return hash;
  }

  @Override
  public int compareTo(Object obj) {
    SearchItem that;
    int result;
    
    if (obj instanceof GopherItem) {
      return ((GopherItem)this).getCompareString().compareTo(
             ((GopherItem)obj).getCompareString());
    }
    
    if (obj instanceof SearchItem) {
      that = (SearchItem)obj;
    } else {
      throw new ClassCastException();
    }    
    result = this.getCompareString().compareTo(that.getCompareString());
    
    return result;
  }

  @Override
  public String getCompareString() {
    return super.getCompareString() + this.getPermanentNameString();
  }
  
  public String getPermanentNameString() {
    return permanentNameString;
  }
  public void setPermanentNameString(String permanentNameString) {
    this.permanentNameString = permanentNameString;
  }
}
