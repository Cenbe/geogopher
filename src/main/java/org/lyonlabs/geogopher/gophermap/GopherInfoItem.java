/**************************************************************************
  GopherInfoItem.java
 **************************************************************************/

package org.lyonlabs.geogopher.gophermap;

/**
 * Subclass of GopherItem for creating info messages.
 */
public class GopherInfoItem extends GopherItem {
  
  /**
   * Constructor for info type gopher items.
   * @param  infoMessage The info message to display.
   */
  public GopherInfoItem(String infoMessage) {
    super(GopherType.INFO, infoMessage, "-", "-", 0);
  }
}
