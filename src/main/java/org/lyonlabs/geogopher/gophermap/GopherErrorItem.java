/**************************************************************************
  GopherErrorItem.java
 **************************************************************************/

package org.lyonlabs.geogopher.gophermap;

/**
 * Subclass of GopherItem for creating error messages.
 */
public class GopherErrorItem extends GopherItem {
  
  /**
   * Constructor for error type gopher items.
   * @param  errorMessage The error message to display.
   */
  public GopherErrorItem(String errorMessage) {
    super(GopherType.ERROR, errorMessage, "-", "-", 0);
  }
}
