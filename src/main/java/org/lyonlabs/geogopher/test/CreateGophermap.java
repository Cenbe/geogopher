/**************************************************************************
  CreateGophermap.java
 **************************************************************************/

package org.lyonlabs.geogopher.test;

import org.lyonlabs.geogopher.gophermap.GopherItem;
import java.io.*;
import java.nio.file.*;
import java.util.*;
import org.apache.logging.log4j.*;
import org.lyonlabs.geogopher.gophermap.GopherItem.GopherType;

/**
 * Create simple gophermap for testing Commodore 64 client.
 */
public class CreateGophermap {
  private final Logger logger = LogManager.getLogger(getClass().getName());
  private final String GOPHERDIR = "/home/cenbe/geoGopher",
                       GOPHERNAME = "gophermap";
  private String host, geoSpecificDir;
  private final int PORT = 6470;
  private enum Machine { greyhand, www, orac, mafarka };
  private static Machine target;
  
  public CreateGophermap() {
    GopherItem item;
    
    logger.info("---------------------");
    logger.info(" CreateTestGophermap");
    logger.info("---------------------");
    
    switch (target) {
      case greyhand:
        host = "greyhand.lyonlabs.org";
        geoSpecificDir = "/download/c64/operating-systems/geos/geoSpecific";
        break;
      case www:
        host = "www.lyonlabs.org";
        geoSpecificDir = "/var/www/html/commodore/onrequest/geos/geoSpecific";
        break;
      case orac:
        host = "orac.lyonlabs.org";
        geoSpecificDir = "/download/c64/operating-systems/geos/geoSpecific";
        break;
      case mafarka:
        host = "mafarka.lyonlabs.org";
        geoSpecificDir = "/download/c64/operating-systems/geos/geoSpecific";
        break;
      default:
        logger.error("Unknown target: " + target);
    }
    logger.info("Building gophermap for " + target + ".");
      
    
    try (BufferedWriter writer = Files.newBufferedWriter(
                        Paths.get(GOPHERDIR, GOPHERNAME),
                        StandardOpenOption.CREATE,
                        StandardOpenOption.TRUNCATE_EXISTING,
                        StandardOpenOption.WRITE,
                        StandardOpenOption.SYNC)) {

      item = new GopherItem(GopherType.INFO, 
                 "Welcome to Cenbe's Commodore 64 gopher server!", "null",
                 host, PORT);
      item.write(writer);

      item = new GopherItem(GopherType.TEXT, 
                 "How to use this gopher site", "how-to-use.txt", 
                 host, PORT);
      item.write(writer);

      item = new GopherItem(GopherType.DIR,
                 "Various GEOS disk images for testing", "geos", host, PORT);
      item.write(writer);
      geosSubdir("geos");  // write gophermap for directory

      item = new GopherItem(GopherType.DIR,
                 "The geoSpecific collection", geoSpecificDir, 
                 host, PORT);
      item.write(writer);
    } catch (Exception exc) {
      System.exit(1);
    }
  }

  private void geosSubdir(String dirName) throws Exception {
    try (BufferedWriter writer = Files.newBufferedWriter(
                        Paths.get(GOPHERDIR, dirName, GOPHERNAME),
                        StandardOpenOption.CREATE,
                        StandardOpenOption.TRUNCATE_EXISTING,
                        StandardOpenOption.WRITE,
                        StandardOpenOption.SYNC)) {
      new GopherItem(GopherType.DIR, "parent directory", "/", host, PORT)
                    .write(writer);
      new GopherItem(GopherType.SEARCH, "Search these GEOS disk images", 
                     GOPHERDIR + "/" + dirName, host, PORT)
                    .write(writer);
      new GopherItem(GopherType.DIR, "The GEOS \"Shadow\" Virus (D64)",
                     "geos/geos-shadow-virus.d64", host, PORT)
                    .write(writer);    
      new GopherItem(GopherType.DIR, "A D81 full of GEOS utilities",
                     "geos/geos-utilities.d81", host, PORT)
                    .write(writer);    
      new GopherItem(GopherType.DIR, "GEOS Programs by Nate Fiedler",
                     "geos/NATEFPRG.D64", host, PORT)
                    .write(writer);
      new GopherItem(GopherType.DIR, "NeoFonts collection (D64)",
                     "geos/NeoFonts-a.d64", host, PORT)
                    .write(writer);    
    } catch (Exception exc) {
      logger.error(exc.getMessage(), exc);
      throw exc;
    }
  }
  
  public static void main(String[] argv) {
    Optional<Machine> machine = Optional.empty();
    
    if (argv.length == 0) {
      usage();
    }
    for (Machine thisMachine : Machine.values()) {
      if (argv[0].equals(thisMachine.toString())) {
        machine = Optional.of(thisMachine);
        break;
      }
    }
    if (machine.isPresent()) {
      target = machine.get();
      new CreateGophermap();
    } else {
      System.out.println("Unknown target: " + target);
      usage();
    }
  }
  
  private static void usage() {
    System.out.println("Usage: CreateTestGophermap <target-machine>\n"
                     + "where <target-machine> is \"greyhand\", \"www\", "
                     + "\"orac\", or \"mafarka\".");
    System.exit(1);
  }
}
