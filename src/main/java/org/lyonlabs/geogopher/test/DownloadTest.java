/**************************************************************************
  DownloadTest.java
 **************************************************************************/

package org.lyonlabs.geogopher.test;

import java.io.*;
import java.net.*;

/**
 * geoGopher file download test.
 */
public class DownloadTest {

  private final String fileUrl
          = "http://www.lyonlabs.org/commodore/onrequest/geos/geowrite-64.d64";

  public DownloadTest() {
    try {
      downloadFromUrl(new URL(fileUrl), "/tmp/geowrite-64.d64");
    } catch (IOException ixc) {
      System.out.println(ixc.getMessage());
      System.exit(1);
    }
  }

  void downloadFromUrl(URL url, String localFilename) throws IOException {
    byte[] buffer;
    int len;

    buffer = new byte[4096];
    URLConnection urlConn = url.openConnection();
    try (InputStream is = urlConn.getInputStream();
         FileOutputStream fos = new FileOutputStream(localFilename)) {
      while ((len = is.read(buffer)) > 0) {
        fos.write(buffer, 0, len);
      }
    }
  }

  public static void main(String[] argv) {
    new DownloadTest();
  }
}
