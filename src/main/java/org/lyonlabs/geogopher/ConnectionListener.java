/**************************************************************************
  ConnectionListener.java
 **************************************************************************/

package org.lyonlabs.geogopher;

import java.io.*;
import java.net.*;
import org.apache.logging.log4j.*;

/**
 * Gopher server intended for Commodore 64 clients using geoGopher. This 
 * class is the connection listener, and spins off a client thread on an 
 * incoming connection. The available command-line parameters are:<ul>
 * <li>-DgopherDirName=&lt;root gopher directory, defaults to user's home dir&gt;
 * <li>-DgopherMapName=&lt;name of gophermap file, defaults to "gophermap"&gt;
 * <li>-DhostName=&lt;host name to use in generated selectors&gt;
 * <li>-Dport=&lt;port number to listen on, defaults to 70&gt;
 * </ul>
 */
public class ConnectionListener {
  private static final Logger logger = LogManager.getLogger(
                       ConnectionListener.class.getName());
  private final String VERSION = "1.03";
  private int port;
  private String hostName, gopherDirName, gopherMapName;
  private File rootGopherDir, rootGopherMap;
  private boolean serving = true;
  
  public ConnectionListener() {    
    logger.info("-----------------------");
    logger.info(" geoGopher server " + VERSION);
    logger.info("-----------------------");

    String log4j2 = System.getProperty("log4j.configurationFile");
    if (log4j2 != null) {
      logger.debug("log4j.configurationFile: " + log4j2);
    }
    
    // pass parameters with -D
    gopherDirName = System.getProperty("gopherDirName");
    if (gopherDirName == null) {
      gopherDirName = System.getProperty("user.dir");
    }
    logger.info("gopherDirName: " + gopherDirName);    
    rootGopherDir = new File(gopherDirName);
    if (!rootGopherDir.exists() || !rootGopherDir.isDirectory() 
    ||  !rootGopherDir.canRead()) {
      logger.error("Can't read gopher directory!");
      System.exit(1);
    }
    
    gopherMapName = System.getProperty("gopherMapName", "gophermap");
    logger.info("gopherMapName: " + gopherMapName);
    rootGopherMap = new File(rootGopherDir, gopherMapName);
    if (!rootGopherMap.exists() || !rootGopherMap.isFile() 
    ||  !rootGopherMap.canRead()) {
      logger.error("Can't read root gophermap!");
      System.exit(1);
    }
    
    try {
      hostName = System.getProperty("hostName", "localhost");
      port = Integer.parseInt(System.getProperty("port", "70"));
      logger.info(hostName + " listening on port " + port);
    } catch (NumberFormatException nxc) {
      logger.error("Invalid port number!");
      System.exit(1);
    }
    
    listenForShutdown();
    serveGopher();
  }

  private void serveGopher() {
    Socket clientSocket;
    String threadName;
    
    logger.info("server: opening server socket");
    try (ServerSocket serverSocket = new ServerSocket(port)) {
      while (serving) {  // FIXME sorcerer's apprentice!
        clientSocket = serverSocket.accept();  // blocks
        logger.info("connect from " + clientSocket.getInetAddress()
                  + " on port " + clientSocket.getPort());
        threadName = clientSocket.getInetAddress() + ":" 
                  + clientSocket.getPort();
        new Thread(new ConnectionThread(this, clientSocket), threadName)
                  .start();
      }
    } catch (Exception exc) {
      logger.error("server: " + exc.getMessage(), exc);
      System.exit(1);
    } finally {
      logger.info("shutting down");
    }
  }
  
  /**
   * Cheap way to terminate server: exit when connection received on 1961.
   * // FIXME Make this more practical and secure.
   */
  private void listenForShutdown() {
    ServerSocket shutdownSocket;
    
    logger.info("server: opening shutdown socket");
    try {
      shutdownSocket = new ServerSocket(1961);
      Runnable shutdownThread = () -> {
        final Socket shutdownRequestSocket;
        try {
          shutdownRequestSocket = shutdownSocket.accept();  // blocks
          logger.info("shutdown from " + shutdownRequestSocket.getInetAddress()
                    + " on port " + shutdownRequestSocket.getPort());
        } catch (Exception exc) {
          logger.error(exc.getMessage(), exc);
        } finally {
          serving = false;
          try {
            // make fake connection so serveGopher() will exit his loop:
            Socket clientSocket = new Socket("localhost", port);
            clientSocket.getOutputStream().write(new byte[]{0x0d, 0x0a});
            clientSocket.close();
            shutdownSocket.close();
          } catch (IOException ixc) {
            logger.error(ixc.getMessage(), ixc);
          }
        }
      };
      new Thread(shutdownThread).start();
    } catch (Exception exc) {
      logger.error("shutdown: " + exc.getMessage(), exc);
      System.exit(1);
    } finally {
    }
  }
  
  public File getRootGopherMap() {
    return rootGopherMap;
  }

  public String getGopherMapName() {
    return gopherMapName;
  }

  public File getRootGopherDir() {
    return rootGopherDir;
  }

  public String getHostName() {
    return hostName;
  }

  public int getPort() {
    return port;
  }

  public static void main(String[] argv) {
    new ConnectionListener();
  }
}
