#!/bin/bash
if [ "$1" == "" ]; then
  JAVA="java "
elif [ "$1" == "debug" ]; then
  JAVA="java -agentlib:jdwp=transport=dt_socket,server=y,suspend=y,address=157.230.158.213:7070 "
fi
JAVADIR=/usr/local/java
$JAVA -DgopherDirName=/var/www/html/commodore/onrequest/geos/geoSpecific \
      -DgopherMapName=gophermap \
      -DhostName=www.lyonlabs.org \
      -Dport=6470 \
      -p $JAVADIR:$JAVADIR/lyonlabs \
      -m org.lyonlabs.geogopher/org.lyonlabs.geogopher.ConnectionListener &
